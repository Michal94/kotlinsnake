package cz.mcapek.snake.model

import cz.mcapek.snake.model.Direction.*
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row

internal class SnakeTest : StringSpec() {
    override fun isInstancePerTest() = true

    init {
        val dimensions = Dimensions(20,30)
        val headCell = Cell(3,3)
        val tailCells = listOf(Cell(4,3), Cell(5,3))
        val snakeCells = headCell + tailCells
        val direction = UP
        val snake = Snake(snakeCells, direction)

        "constructor should require non empty cells" {
            shouldThrow<IllegalArgumentException> { Snake(listOf(), direction) }
        }

        "snake shold return first cell as head" {
            snake.head shouldBe headCell
        }

        "snake should return all cells except the first as tail" {
            snake.tail shouldBe tailCells
        }

        "snake should move in given direction and keep that direction" {
            forall(
                row(Snake(listOf(Cell(2,3), Cell(3,3), Cell(4,3)), LEFT), LEFT),
                row(Snake(listOf(Cell(3,2), Cell(3,3), Cell(4,3)), UP), UP),
                row(Snake(listOf(Cell(3, 4), Cell(3,3), Cell(4,3)), DOWN), DOWN)
            ) { expectedSnake, direction ->
                Snake(snakeCells, direction).move() shouldBe expectedSnake
            }
        }

        "snake should throw exception if direction reverses the snake " {
            shouldThrow<IllegalArgumentException> { Snake(snakeCells, RIGHT) }
        }

        "snake should turn if new direction is perpendicular to the old one" {
            forall(
                row(LEFT, LEFT, RIGHT),
                row(UP, LEFT, UP),
                row(LEFT, LEFT, LEFT)
            ) {expectedDirection, originalDirection, newDirection ->
                Snake(snakeCells, originalDirection).turn(newDirection) shouldBe Snake(snakeCells,expectedDirection)

            }
        }

        "snake and apples should be the same if snake ate no apples" {
            val apples = Apples(setOf(),dimensions)

            snake.eat(apples) shouldBe Pair(snake, apples)
        }

        "snake should return apples without the eaten one" {
            val apples = Apples(setOf(headCell),dimensions)

            snake.eat(apples).apples shouldBe Apples(setOf(), dimensions)
        }

        "snake should return  with incremented future growth if it ate an apple" {
            val apples = Apples(setOf(headCell),dimensions)

            snake.eat(apples).snake shouldBe snake.copy(futureGrowth = 1)
        }

        "snake should throw exception if future growth is negative" {
            shouldThrow<IllegalArgumentException> {
                Snake(snakeCells, direction, -1)
            }
        }

        "snake should grow tail on next move if future growth is bigger than zero" {
            snake.copy(futureGrowth = 1).move() shouldBe
                    snake.copy(
                        cells = Cell(3,2) + snakeCells,
                        futureGrowth = 0
                    )
        }
    }
}