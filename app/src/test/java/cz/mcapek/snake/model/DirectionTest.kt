package cz.mcapek.snake.model

import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row

internal class DirectionTest : StringSpec() {
    override fun isInstancePerTest() = true

    init {
        "direction should have correct dx and dy" {
            forall(
                row(0, -1, Direction.UP),
                row(0, 1, Direction.DOWN),
                row(1, 0, Direction.RIGHT),
                row(-1, 0, Direction.LEFT)
            ) {expectedDx, expectedDy, direction ->
                direction.dx shouldBe expectedDx
                direction.dy shouldBe expectedDy
            }
        }
        "is perpendicular should return correc result" {
            forall(
                row(false, Direction.UP, Direction.DOWN),
                row(false, Direction.LEFT, Direction.RIGHT),
                row(true, Direction.UP, Direction.RIGHT),
                row(true, Direction.UP, Direction.LEFT)
            ) {expected, direction1, direction2 ->
                direction1 isPerpendicularTo direction2 shouldBe expected
            }
        }
    }
}