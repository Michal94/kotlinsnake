package cz.mcapek.snake.model

import arrow.core.Right
import cz.mcapek.snake.R.id.dimensions
import cz.mcapek.snake.model.Direction.*
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verifyOrder

internal class GameStateUpdaterTest : StringSpec() {
    override fun isInstancePerTest() = true

    init {
        val dimensions = Dimensions(20,30)
        val newSnake = Snake(cells = (0..2).map { Cell(it, 0) },
            direction = Direction.LEFT
        )
        val newApples = Apples(cells = setOf(Cell(0,0)), field = dimensions)
        val snake = spyk(Snake(
            cells = (1..3).map {Cell(it,0)},
            direction = Direction.LEFT
        )) {
            every {move()} returns this
            every {turn(any())} returns this
            every {eat(any())} returns Pair(newSnake, newApples)
        }
        val apples = mockk<Apples>(){
            every { grow() } returns newApples
            every { cells } returns setOf()
        }
        val runningState = GameState.Running(
            field = dimensions,
            snake = snake,
            apples = apples
        )
        val direction = Direction.RIGHT
        val updater = GameStateUpdater(runningState)
        "should update from waiting state to running state" {
            updater.update(GameState.Waiting, direction) shouldBe runningState
        }

        "snake should turn, move and eat grown apples on update in running state"{
            updater.update(runningState, direction)

            verifyOrder {
                snake.turn(direction)
                snake.move()
                snake.eat(newApples)
            }
        }

        "updater should return new running state with correct snake and apples"{
            updater.update(runningState, direction) shouldBe
                    runningState.copy(snake = newSnake, apples = newApples)
        }



        fun testOverState(cells: List<Cell>, direction: Direction) {
            val stateBeforeDeath = GameState.Running(
                dimensions,
                Snake(cells, direction),
                apples
            )

            updater.update(stateBeforeDeath, direction).shouldBeInstanceOf<GameState.Over>()
        }
        "updater should over state if snake runs into top wall" {
            testOverState(listOf(Cell(1,0)), UP)
        }

        "updater should over state if snake runs into bottom wall" {
            testOverState(listOf(Cell(1,dimensions.height-1)), DOWN)
        }

        "updater should over state if snake runs into left wall" {
            testOverState(listOf(Cell(0,1)), LEFT)
        }

        "updater should over state if snake runs into right wall" {
            testOverState(listOf(Cell(dimensions.width - 1,1)), RIGHT)
        }

        "updater should return over state if snake runs into itself" {
            testOverState(listOf(Cell(0,1), Cell(0,0), Cell(1,0), Cell(1,1), Cell(1,2)), RIGHT)
        }

        "updater should return over state on update from over state" {
            val anyOverState = GameState.Over(123)

            updater.update(anyOverState, direction) shouldBe anyOverState
        }
    }
}