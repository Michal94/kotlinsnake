package cz.mcapek.snake.model

import io.kotlintest.data.forall
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row


internal class CellTest : StringSpec() {
    override fun isInstancePerTest() = true

    init {
        "cell should move in right direction" {
            forall(
                row(Cell(4,3), Direction.LEFT),
                row(Cell(6,3), Direction.RIGHT),
                row(Cell(5,4), Direction.DOWN),
                row(Cell(5,2), Direction.UP)
            ) { expectedCell, direction ->
                Cell(5,3).move(direction) shouldBe expectedCell

            }
        }

        "plus operator should return list of this and other cells" {
            Cell(0,0) + listOf(Cell(1,1), Cell(2,2)) shouldContainExactly listOf(Cell(0,0),Cell(1,1),Cell(2,2))
        }
    }
}