package cz.mcapek.snake.di

import android.content.Context
import cz.mcapek.snake.system.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [GameModule::class])
interface GameComponent {

    fun inject(gameActivity: MainActivity)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun applicationContext(context: Context): Builder

        fun build(): GameComponent
    }
}