package cz.mcapek.snake.di

import android.os.Vibrator
import cz.jhutarek.snake.game.device.TickerImpl
import cz.mcapek.snake.domain.Game
import cz.mcapek.snake.domain.Ticker
import cz.mcapek.snake.model.*
import cz.mcapek.snake.system.BoardView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class GameModule {

    companion object {
        private val FIELD_DIMENSIONS = Dimensions(20, 20)
        private val INITIAL_SNAKE = Snake((14..18).map { Cell(it, 10) }, Direction.LEFT)
        private const val INTERVAL = 150L
    }

    @Provides
    @Singleton
    fun stateUpdater(): GameStateUpdater = GameStateUpdater(
        GameState.Running(
            field = FIELD_DIMENSIONS,
            snake = INITIAL_SNAKE,
            apples = Apples(field = FIELD_DIMENSIONS, cells = setOf())
        )
    )


    @Provides
    @Singleton
    fun ticker(tickerImpl: TickerImpl): Ticker = tickerImpl


}