package cz.mcapek.snake.model

data class Dimensions(val width: Int, val height: Int)