package cz.mcapek.snake.model

sealed class GameState {
    object Waiting: GameState()

    data class Running(
        val field: Dimensions,
        val snake: Snake,
        val apples: Apples
    ) : GameState() {
        val score = snake.size * 100
    }

    data class Over(
        val score: Int
    ) : GameState()
}