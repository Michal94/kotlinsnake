package cz.mcapek.snake.model

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameStateUpdater @Inject constructor(val initialRunningState: GameState.Running) {

    fun update(previous: GameState, direction: Direction) = when(previous) {
        is GameState.Waiting -> initialRunningState
        is GameState.Running -> previous.snake
            .turn(direction)
            .move()
            .eat(previous.apples.grow())
            .let {
                val (newSnake, newApples) = it

                if ( newSnake.head in newSnake.tail ||newSnake.head !in previous.field) GameState.Over(previous.score) //TODO
                else previous.copy(snake = newSnake, apples = newApples)
            }
        is GameState.Over -> previous
    }

    private operator fun Dimensions.contains(cell: Cell) =
            cell.x in 0 until width && cell.y in 0 until height
}