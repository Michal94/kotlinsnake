package cz.mcapek.snake.model

import kotlin.random.Random

data class Apples (
    val cells: Set<Cell>,
    private val field: Dimensions,
    private val maxApples: Int = 3,
    private  val growthProbability: Double = 0.1,
    private val randomGenerator: Random = Random
){
    fun grow() =
            if(mustGrow) copy(cells = cells + field.getRandomCell())
            else this

    private val mustGrow get() = cells.size < maxApples && randomGenerator.nextDouble() < growthProbability

    private fun Dimensions.getRandomCell() = Cell(
        randomGenerator.nextInt(width),
        randomGenerator.nextInt(height)
    )
}