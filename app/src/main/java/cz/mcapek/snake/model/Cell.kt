package cz.mcapek.snake.model

data class Cell(val x: Int, val y: Int) {
    fun move(direction: Direction) = copy(
        x = x + direction.dx,
        y = y + direction.dy
    )

    operator fun plus(other: List<Cell>) = listOf(this) + other
}