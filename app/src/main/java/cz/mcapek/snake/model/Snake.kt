package cz.mcapek.snake.model

data class Snake(
    val cells: List<Cell>,
    val direction: Direction,
    private val futureGrowth: Int = 0
) {
    init {
        require(cells.isNotEmpty()) {"Cells must be not empty"}
        require(head.move(direction) != tail.firstOrNull()) {"Direction must not reverse the snake"}
        require(futureGrowth >= 0) {"Future growth must not be negative"}
    }

    val head get() = cells.first()
    val tail get() = cells.drop(1)
    val size get() = cells.size

    fun move(): Snake {
        val newHead = head.move(direction)
        val newTail = if (futureGrowth > 0) cells else cells.dropLast(1)

        return copy(
            cells = newHead + newTail,
            futureGrowth = (futureGrowth - 1).coerceAtLeast(0)
        )
    }

    fun turn(newDirection: Direction) =
            if (direction isPerpendicularTo newDirection) copy(direction = newDirection)
    else this

    fun eat(apples: Apples) =
            if (head in apples.cells) Pair(
                this.copy(futureGrowth = 1),
                apples.copy(cells = apples.cells - head)
            ) else Pair(
                this,
                apples
            )
}

inline val Pair<Snake, Apples>.snake get() = first
inline val Pair<Snake, Apples>.apples get() = second