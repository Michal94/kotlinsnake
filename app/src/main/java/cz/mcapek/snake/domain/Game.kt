package cz.mcapek.snake.domain

import cz.mcapek.snake.model.Direction
import cz.mcapek.snake.model.GameState
import cz.mcapek.snake.model.GameStateUpdater
import javax.inject.Inject
import javax.inject.Singleton

typealias GameListener = (GameState) -> Unit

@Singleton
class Game @Inject constructor(
    private val ticker: Ticker,
    private val stateUpdater: GameStateUpdater
) {
    companion object {
        const val TICKER_INTERVAL_MILLIS = 150L
    }

    init {
        ticker.listener = { update() }
    }

    private var state: GameState = GameState.Waiting
        set(value) {
            field = value
            listener?.invoke(field)
        }


    var listener: GameListener? = null
        set(value) {
            field = value
            listener?.invoke(state)
        }

    private val defaultDirection = stateUpdater.initialRunningState.snake.direction
    var direction : Direction = defaultDirection

    fun reset(){
        ticker.stop()
        state = GameState.Waiting
    }

    fun start(){
        direction = defaultDirection
        ticker.start(TICKER_INTERVAL_MILLIS)
    }

    private fun update(){
        state = stateUpdater.update(state,direction)

        if (state is GameState.Over){
            ticker.stop()
        }
    }

}