package cz.mcapek.snake.domain

typealias TickerListener = () -> Unit

interface Ticker {
    var listener: TickerListener?

    fun start(intervalMillis: Long)

    fun stop()
}