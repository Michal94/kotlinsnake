package cz.mcapek.snake.presentation

import android.view.View
import cz.mcapek.snake.domain.Game
import cz.mcapek.snake.model.*
import javax.inject.Inject
import javax.inject.Singleton

typealias GameViewModelListener = (GameViewModel.ViewState) -> Unit

@Singleton
class GameViewModel @Inject constructor(
    private val game: Game
) {
    init {
        game.listener = { update(it) }
    }

    data class ViewState (
        val introVisible: Boolean,
        val gameVisible: Boolean,
        val overVisible: Boolean,
        val field: Dimensions?,
        val snake: List<Cell>?,
        val apples: Set<Cell>?,
        val score: String?
    )

    var listener: GameViewModelListener? = null
    set(value) {
        field = value
        field?.invoke(state)
    }

    private var state = ViewState(
        introVisible = true,
        gameVisible = false,
        overVisible = false,
        field = null,
        snake = null,
        apples = null,
        score = null
    )
    set(value){
        field = value
        listener?.invoke(field)
    }

    fun reset(){
        game.reset()
    }

    fun start(){
        game.start()
    }

    fun swipe(from: Point, to: Point){
        val (dx,dy) = abs(from - to)

        game.direction = when {
            dx > dy -> when {
                from.x < to.x -> Direction.RIGHT
                else -> Direction.LEFT
            }
            else -> when {
                from.y < to.y -> Direction.DOWN
                else -> Direction.UP
            }
        }
    }

    private fun update(gameState: GameState){
       state = when(gameState){
           is GameState.Waiting -> ViewState(
               introVisible = true,
               gameVisible = false,
               overVisible = false,
               field = null,
               snake = null,
               apples = null,
               score = null
           )
           is GameState.Running -> ViewState(
               introVisible = false,
               gameVisible = true,
               overVisible = false,
               field = gameState.field,
               snake = gameState.snake.cells,
               apples = gameState.apples.cells,
               score = gameState.score.toString()
           )
           is GameState.Over -> ViewState(
               introVisible = false,
               gameVisible = false,
               overVisible = true,
               field = null,
               snake = null,
               apples = null,
               score = gameState.score.toString()
           )
       }
    }
}