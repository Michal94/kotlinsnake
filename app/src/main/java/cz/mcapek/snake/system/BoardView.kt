package cz.mcapek.snake.system

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import cz.mcapek.snake.model.Cell
import cz.mcapek.snake.model.Dimensions

class BoardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): View(context,attrs,defStyleAttr) {

    data class State(
        val field: Dimensions,
        val snake: List<Cell>,
        val apples: Set<Cell>
        )

    private val backgroundPaint = Paint().apply {
        color = Color.parseColor("#ffffff00")
        style = Paint.Style.FILL
    }

    private val foregroundPaint = Paint().apply {
        color = Color.parseColor("#ff333333")
        style = Paint.Style.FILL
    }

    private var gridSize: Float = 0f

    var state: State? = null
    set(value) {
        field = value
        updateGridSize()
        invalidate()
    }

    private fun updateGridSize() {
        gridSize = state?.let {
            minOf(
                width.toFloat() / it.field.width,
                height.toFloat() / it.field.height
            )
        } ?: 0f
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        updateGridSize()
    }

    override fun onDraw(canvas: Canvas) {
        state?.let {
            drawBackground(canvas, it)
            drawApples(canvas, it)
            drawSnake(canvas, it)
        }
    }

    private fun drawSnake(canvas: Canvas, board: State) {
        board.snake.forEach {
            canvas.drawRect(
                it.x * gridSize, it.y * gridSize,
                (it.x + 1) * gridSize, (it.y + 1) * gridSize,
                foregroundPaint
            )
        }
    }

    private fun drawApples(canvas: Canvas, board: State) {
        val gridThirdSize = gridSize / 3f

        board.apples.forEach {
            canvas.drawRect(
                it.x * gridSize + gridThirdSize, it.y * gridSize,
                it.x * gridSize + gridThirdSize * 2, it.y * gridSize + gridThirdSize,
                foregroundPaint
            )
            canvas.drawRect(
                it.x * gridSize, it.y * gridSize + gridThirdSize,
                it.x * gridSize + gridThirdSize, it.y * gridSize + gridThirdSize * 2,
                foregroundPaint
            )
            canvas.drawRect(
                it.x * gridSize + gridThirdSize, it.y * gridSize + gridThirdSize * 2,
                it.x * gridSize + gridThirdSize * 2, (it.y + 1) * gridSize,
                foregroundPaint
            )
            canvas.drawRect(
                it.x * gridSize + gridThirdSize * 2, it.y * gridSize + gridThirdSize,
                (it.x + 1) * gridSize, it.y * gridSize + gridThirdSize * 2,
                foregroundPaint
            )
        }
    }

    private fun drawBackground(canvas: Canvas, board: State) {
        canvas.drawRect(
            0f, 0f,
            board.field.width * gridSize,
            board.field.height * gridSize,
            backgroundPaint
        )
    }
}