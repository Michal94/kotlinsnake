package cz.mcapek.snake.system

import android.app.Application
import android.content.Context
import cz.mcapek.snake.di.DaggerGameComponent
import cz.mcapek.snake.di.GameComponent

class Application : Application() {

    companion object {
        fun getInjector(context: Context) = ((context.applicationContext) as? cz.mcapek.snake.system.Application)
            ?.component
            ?: throw IllegalStateException("Cannot obtain injector")
    }

    private lateinit var component: GameComponent

    override fun onCreate() {
        super.onCreate()

        component = DaggerGameComponent.builder()
            .applicationContext(this)
            .build()
    }
}