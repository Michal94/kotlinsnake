package cz.mcapek.snake.system

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import cz.mcapek.snake.R
import cz.mcapek.snake.model.Point
import cz.mcapek.snake.presentation.GameViewModel
import kotlinx.android.synthetic.main.game__game_include.*
import kotlinx.android.synthetic.main.game__intro_include.*
import kotlinx.android.synthetic.main.game__over_include.*
import kotlinx.android.synthetic.main.game_game_activity.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val gestureDetector by lazy {
        GestureDetectorCompat(this, object : GestureDetector.SimpleOnGestureListener() {
            override fun onDown(e: MotionEvent) = true

            override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
                viewModel.swipe(e1.toPoint(), e2.toPoint())
                return true
            }
        })
    }

    private fun MotionEvent.toPoint() = Point(x,y)

    @Inject internal lateinit var viewModel: GameViewModel

    private var isGameRunning: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Application.getInjector(this).inject(this)

        setContentView(R.layout.game_game_activity)

        start.setOnClickListener { viewModel.start() }
        reset.setOnClickListener { viewModel.reset() }


    }

    override fun onStart() {
        super.onStart()

        viewModel.listener = {
            intro.visibility = if (it.introVisible) View.VISIBLE else View.GONE
            game.visibility = if (it.gameVisible) View.VISIBLE else View.GONE
            over.visibility = if (it.overVisible) View.VISIBLE else View.GONE
            board.state =
                    if (it.field != null && it.snake != null && it.apples != null) BoardView.State(
                        it.field,
                        it.snake,
                        it.apples
                    )
                    else null
            gameScore.text = it.score
            overScore.text = it.score
            isGameRunning = it.gameVisible
        }
    }

    override fun onStop() {
        super.onStop()

        viewModel.listener = null
    }

    override fun onTouchEvent(event: MotionEvent) =
        if (isGameRunning) gestureDetector.onTouchEvent(event)
    else false
}
